package main

import "fmt"


//Single line comment

/*Multi 
	Line
	comment */


//Function declaration
//The name "main" is special. It is the function that is called upon execution.
func main() {
    fmt.Printf("hello, world\n")
}
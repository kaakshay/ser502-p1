package main

import "fmt"

//Variables and control flow.

func main() {

	// Variable declaration and assignment

	var x string = "Hello world"
    fmt.Println(x)

    var i int
    //if statement
    i=5
    if i % 2 == 0 {
	    // divisible by 2
	} else if i % 3 == 0 {
	    // divisible by 3
	} else if i % 4 == 0 {
	    // divisible by 4
	}

    //for loop
    
    for i := 1; i <= 10; i++ {
        fmt.Println(i)
    }

    //Special form of for loop.
    //In this for loop i represents the current position in the array and value is 
    //the same as x[i]. We use the keyword range followed by the name of the variable 
    //we want to loop over.
    /*var total float64 = 0
	for i, value := range x {
	    total += value
	}
	fmt.Println(total / float64(len(x)))*/
    //In the example below, the compiler will throw an error because i is never used.
    //So "_" is used to tell the compiler that we don't need this
    
    var total float64 = 0
    //var arr[5]float64
   	arr := [5]float64{1, 2, 3, 4, 5}
	for _, value := range arr {
	    total += value
	}
	fmt.Println(total / float64(len(x)))

}
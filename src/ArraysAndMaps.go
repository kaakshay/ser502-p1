package main

import "fmt"

//Arrays, Slices and Maps

func main() {
    //Arrays
    var x [5]int
    x[4] = 100
    fmt.Println(x)
    // This prints out [0 0 0 0 100]

    fmt.Println(len(x))
    //This prints out the length of the array ie. 5

    //Map with string key and int value
    testMap := make(map[string]int)
	//Inserting
	testMap["key"] = 10
	fmt.Println(testMap["key"])
	//Deleting
	delete(testMap, "key")
	fmt.Println(testMap["key"])
}
package main

import "fmt"

//goroutines

func main() {

/*This is why the call to the Scanln function has been included; without it the 
    program would exit before being given the opportunity to print all the numbers.*/
go f(0)
    var input string
    fmt.Scanln(&input)
}

 func f(n int) {
    for i := 0; i < 10; i++ {
       fmt.Println(n, ":", i)
   }
}